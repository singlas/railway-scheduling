import copy

from config import LINE_TYPE_WASHLINE, LINE_TYPE_PLATFORM, LINE_TYPE_STABLING, PLATFORM_WAIT_TIME, ON_STATION_LIMIT


class Train:

    def __init__(self, id, length, train_type, rake_type, in_time, out_time):
        self.id = id
        self.size = length
        self.type = train_type
        self.rake_type = rake_type
        self.in_time = in_time
        self.out_time = out_time
        self.complete_task_list = []
        self.task_list = ["WASHER", "WASH_LINE", "IN_STATION", "OUT_STATION", "IDLE"]
        self.transition_count = 0

        self.schedule = {
            "wash_time": 0,
            "idle_slots": {},
            "idle_slots_line": {},
        }

        self.init_train_type_based_vars()

    def init_train_type_based_vars(self):

        if self.type in ["P", "S"]:
            self.schedule["in_station_time"] = (self.in_time, self.in_time + PLATFORM_WAIT_TIME)
            self.schedule["out_station_time"] = (self.out_time - PLATFORM_WAIT_TIME, self.out_time)
            self.schedule["wash_time"] = 4 * 60 if self.type == "S" else 6 * 60
            self.transition_count = 2

        else:
            if self.out_time - self.in_time <= ON_STATION_LIMIT:
                self.schedule["in_station_time"] = (self.in_time, self.out_time)
                self.task_list = ["IN_STATION"]
                self.transition_count = 0
                # no idle slots and no idling task required
            else:
                self.schedule["in_station_time"] = (self.in_time, self.in_time + PLATFORM_WAIT_TIME)
                self.schedule["out_station_time"] = (self.out_time - PLATFORM_WAIT_TIME, self.out_time)
                self.task_list = ["IN_STATION", "OUT_STATION", "IDLE"]
                self.schedule['idle_slots']['IN_TO_OUT'] = (
                    self.schedule["in_station_time"][1], self.schedule["out_station_time"][0])
                self.transition_count = 1

                # self.schedule = Schedule(id)

    def __str__(self):
        return "Id: {} from {} to {}".format(self.id, self.in_time, self.out_time)

    def __len__(self):
        """
        A string representation of this map
        :return: A string representing this map
        """
        return self.size

    @property
    def washer_start_time(self):
        return self.schedule['washer_slot'][0][0]

    @property
    def washer_end_time(self):
        return self.schedule['washer_slot'][0][1]

    @property
    def wash_time(self):
        return self.schedule['wash_time']

    @property
    def idle_slots(self):
        return self.schedule['idle_slots']

    def is_valid_task(self, task):
        return task in self.task_list

    def is_task_complete(self, task):
        return task in self.complete_task_list

    def is_train_processed(self):
        return all(self.is_task_complete(x) for x in self.task_list)

    def add_idle_slots_after_washer(self):
        if self.schedule["in_station_time"][1] != self.washer_start_time:
            self.schedule['idle_slots']['IN_TO_WASH'] = (self.schedule["in_station_time"][1], self.washer_start_time)

        if self.washer_end_time != self.schedule["out_station_time"][0]:
            self.schedule['idle_slots']['WASH_TO_OUT'] = (self.washer_end_time, self.schedule["out_station_time"][0])


class TrainManager:

    def __init__(self, trains_data, platform_manager, washer_manager):
        self.trains = []
        self.pm = platform_manager
        self.wm = washer_manager

        for train in trains_data:
            self.trains.append(Train(train['in_train'], train['length'],
                                     train['type'], train['rake_type'],
                                     train['in_time'], train['out_time']))

        self.sort_trains_on_priority()

    def __len__(self):
        """
        A string representation of this map
        :return: A string representing this map
        """
        return len(self.trains)

    def is_train_processed(self, train_index):
        train = self.trains[train_index]
        return train.is_train_processed()

    def sort_trains_on_priority(self, reverse=False):
        self.trains.sort(key=lambda x: x.out_time - x.in_time - x.wash_time - 60, reverse=reverse)

    # def is_train_processed(self, train_index):
    #     return self.trains[train_index].is_processed()

    def is_valid_task(self, train_index, task):
        train = self.trains[train_index]
        return train.is_valid_task(task)

    def is_task_complete(self, train_index, task):
        train = self.trains[train_index]
        return train.is_task_complete(task)

    def get_options_for_task(self, train_index, task):
        # function should only be called for valid tasks
        assert (self.is_valid_task(train_index, task))

        if task == "WASHER":
            return self.get_available_washers(train_index)

        if task == "WASH_LINE":
            return self.get_available_washlines(train_index)

        if task == "IN_STATION":
            return self.get_available_stations(train_index)

        if task == "OUT_STATION":
            return self.get_available_stations(train_index, out=True)

        if task == "IDLE":
            return self.get_available_lines_for_idling(train_index)

    def do_task_with_option(self, train_index, task, opt):

        if task == "WASHER":
            self.assign_washer(train_index, opt)

        if task == "WASH_LINE":
            self.assign_washline(train_index, opt)

        if task == "IN_STATION":
            self.assign_station(train_index, opt)

        if task == "OUT_STATION":
            self.assign_station(train_index, opt, out=True)

        if task == "IDLE":
            self.assign_lines_for_idling(train_index, opt)

        self.trains[train_index].complete_task_list.append(task)

    def get_task_chaos(self, task):

        if task == "WASHER":
            return self.wm.get_chaos()

        if task == "WASH_LINE":
            return self.pm.get_chaos(LINE_TYPE_WASHLINE)

        if task == "IN_STATION":
            return sum([train.transition_count for train in self.trains])
            # return   # self.pm.get_chaos(LINE_TYPE_PLATFORM)

    def get_available_washers(self, train_index):
        train = self.trains[train_index]
        available_washers_sched = self.wm.get_available_washers(train.wash_time,
                                                                train.in_time + PLATFORM_WAIT_TIME,
                                                                train.out_time - PLATFORM_WAIT_TIME)
        return available_washers_sched

    def get_available_washlines(self, train_index):
        train = self.trains[train_index]
        available_washlines = self.pm.get_available_washlines(train, train.washer_start_time, train.washer_end_time)
        return available_washlines

    def get_available_schedules(self, train_index, out=False):
        train = self.trains[train_index]

        train_time = train.schedule['out_station_time'] if out else train.schedule['in_station_time']

        available_platforms = self.pm.get_available_platforms(train_time[0], train_time[1], train)
        return available_platforms

    def get_available_stations(self, train_index, out=False):
        train = self.trains[train_index]
        train_time = train.schedule['out_station_time'] if out else train.schedule['in_station_time']

        available_platforms = self.pm.get_available_platforms(train_time[0], train_time[1], train)
        return available_platforms

    def get_available_lines_for_idling(self, train_index):
        options = []
        train = self.trains[train_index]

        in_platform_index = train.schedule['in_station']
        in_platform = self.pm.platforms[in_platform_index]

        out_platform_index = train.schedule['out_station']
        out_platform = self.pm.platforms[out_platform_index]

        # if train has 0 wash times, we have to fill only one slot
        if 'IN_TO_OUT' in train.idle_slots:
            slot = train.idle_slots['IN_TO_OUT']
            platform_available_for_slot = []

            if in_platform.is_available(slot[0], slot[1]):
                platform_available_for_slot.append({in_platform_index: [LINE_TYPE_PLATFORM, slot[0], slot[1]]})

            if out_platform.is_available(slot[0], slot[1]):
                platform_available_for_slot.append({out_platform_index: [LINE_TYPE_PLATFORM, slot[0], slot[1]]})

            for line_index in self.pm.get_available_stabling_lines(slot[0], slot[1], train,
                                                                   [in_platform_index, out_platform_index]):
                platform_available_for_slot.append({line_index: [LINE_TYPE_STABLING, slot[0], slot[1]]})

            options = [{'IN_TO_OUT': x} for x in platform_available_for_slot]

        else:
            wash_platform_index = train.schedule['washline']
            wash_platform = self.pm.washlines[wash_platform_index]

            if 'IN_TO_WASH' in train.idle_slots:

                slot1 = train.idle_slots['IN_TO_WASH']
                full_options_for_slot1 = []
                option = {}

                # if the train has free space on the washline, assign that much
                wash_platform_slot = wash_platform.get_available_sub_slot_from_end(slot1[0], slot1[1])
                if wash_platform_slot:
                    option[wash_platform_index] = [LINE_TYPE_WASHLINE, wash_platform_slot[0], wash_platform_slot[1]]

                if wash_platform_slot and wash_platform_slot[0] == slot1[0]:
                    full_options_for_slot1.append(option)
                else:
                    remaining_end = wash_platform_slot[0] if wash_platform_slot else slot1[1]

                    # Next add an option for all remaining time on platform
                    if in_platform.is_available(slot1[0], remaining_end):
                        current_option = copy.deepcopy(option)
                        current_option[in_platform_index] = [LINE_TYPE_PLATFORM, slot1[0], remaining_end]
                        full_options_for_slot1.append(current_option)

                    for line_index in self.pm.get_available_stabling_lines(slot1[0], remaining_end, train,
                                                                           [in_platform_index]):
                        current_option = copy.deepcopy(option)
                        current_option[line_index] = [LINE_TYPE_STABLING, slot1[0], remaining_end]
                        full_options_for_slot1.append(current_option)

                options = [{'IN_TO_WASH': x} for x in full_options_for_slot1]

            if 'WASH_TO_OUT' in train.idle_slots:

                slot2 = train.idle_slots['WASH_TO_OUT']
                full_options_for_slot2 = []
                option = {}

                # if the train has free space on the washline, assign that much
                wash_platform_slot = wash_platform.get_available_sub_slot_from_start(slot2[0], slot2[1])
                if wash_platform_slot:
                    option[wash_platform_index] = [LINE_TYPE_WASHLINE, wash_platform_slot[0], wash_platform_slot[1]]

                if wash_platform_slot and wash_platform_slot[1] == slot2[1]:
                    full_options_for_slot2.append(option)
                else:
                    remaining_start = wash_platform_slot[1] if wash_platform_slot else slot1[0]

                    # Next add an option for all remaining time on platform
                    if out_platform.is_available(remaining_start, slot2[1]):
                        current_option = copy.deepcopy(option)
                        current_option[out_platform_index] = [LINE_TYPE_PLATFORM, remaining_start, slot2[1]]
                        full_options_for_slot2.append(current_option)

                    for line_index in self.pm.get_available_stabling_lines(remaining_start, slot2[1], train,
                                                                           [out_platform_index]):
                        current_option = copy.deepcopy(option)
                        current_option[line_index] = [LINE_TYPE_STABLING, remaining_start, slot2[1]]
                        full_options_for_slot2.append(current_option)

                options = [{'WASH_TO_OUT': x} for x in full_options_for_slot2]

                # platform_available_for_slot2 = []
                #
                # if out_platform.is_available(slot2[0], slot2[1]):
                #     platform_available_for_slot2.append((out_platform_index, LINE_TYPE_PLATFORM))
                #
                # if wash_platform.is_available(slot2[0], slot2[1]):
                #     platform_available_for_slot2.append((wash_platform_index, LINE_TYPE_WASHLINE))
                #
                # for line_index in self.pm.get_available_stabling_lines(slot2[0], slot2[1], train, [out_platform_index]):
                #     platform_available_for_slot2.append((line_index, LINE_TYPE_STABLING))
                #
                # options = [{'WASH_TO_OUT': x} for x in platform_available_for_slot2]

            if 'IN_TO_WASH' in train.idle_slots and 'WASH_TO_OUT' in train.idle_slots:
                options = [{'IN_TO_WASH': x, 'WASH_TO_OUT': y} for x in full_options_for_slot1 for y in
                           full_options_for_slot2]

        options.sort(key=lambda opt: sum(self.get_priority_score_for_option(x) for x in opt.values()), reverse=True)

        return options

    def get_priority_score_for_option(self, option):
        """
        :param option: is a dictionary of line index: [platform type, start time, end time]
        :return:
        """
        score = 0
        for line_data in option.values():
            # if line_data[0] == LINE_TYPE_PLATFORM:
            #     score += 2 if (line_data[2] - line_data[1]) < 20 else 0

            if line_data[0] == LINE_TYPE_STABLING:
                score += 1

        return score

    def assign_washer(self, train_index, washer_with_sched):
        train = self.trains[train_index]
        train.schedule["washer_slot"] = washer_with_sched
        self.wm.assign_washer(washer_with_sched, train)

        # add idle slots
        train.add_idle_slots_after_washer()

    def assign_washline(self, train_index, washline_index):
        train = self.trains[train_index]
        train.schedule['washline'] = washline_index
        # train.add_train_task(train.in_time,train.in_time + PLATFORM_WAIT_TIME, TASK_LIST[2])
        self.pm.assign_line(washline_index, train.washer_start_time, train.washer_end_time, train.id,
                            LINE_TYPE_WASHLINE, event_details=train.schedule['washer_slot'][1])

    def assign_station(self, train_index, platform_index, out=False):
        train = self.trains[train_index]
        train_time = train.schedule['out_station_time'] if out else train.schedule['in_station_time']
        train.schedule["out_station" if out else "in_station"] = platform_index

        # train.add_train_task(train_time[0], train_time[1], TASK_LIST[3 if out else 2])
        self.pm.assign_line(platform_index, train_time[0], train_time[1], train.id, LINE_TYPE_PLATFORM)

    def assign_lines_for_idling(self, train_index, opt):
        train = self.trains[train_index]
        train.schedule['idle_slots_lines'] = opt

        for slot_index, platform_dict in opt.items():
            for platform_index, platform_vals in platform_dict.items():
                if platform_vals[0] == LINE_TYPE_STABLING:
                    train.transition_count += 1
                self.pm.assign_line(platform_index, platform_vals[1], platform_vals[2], train.id, platform_vals[0])

    def get_secondary_chaos(self, task):

        if task == "WASHER":
            min_idling_one_side = 0

            for train in self.trains:
                if train.is_valid_task(task):
                    min_idling_one_side = min(train.washer_start_time - train.schedule["in_station_time"][1],
                                              train.schedule["out_station_time"][0] - train.washer_end_time)

            return min_idling_one_side

        return 0
