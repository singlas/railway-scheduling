from schedule import Schedule
from config import TRAIN_NUMBER_OF_DAYS, SCHEDULE_NUMBER_OF_DAYS, MINS_IN_DAY, WASH_INTERVAL
from washer_utils import combine_small_slots, get_slot_wash_time, get_slot_of_wash_time_given_start, \
    get_slot_of_wash_time_given_end

slot = []


class Washer:

    def __init__(self, id, in_time, out_time, days):
        self.id = id
        # self.in_time = in_time * 60
        # self._out_time = out_time * 60
        self.schedule = Schedule(id)

        self.work_times = []
        self.add_work_times_with_round_robin(in_time * 60, out_time * 60)

    def add_work_times_with_round_robin(self, start_time, end_time):

        end_time = end_time if end_time > start_time else end_time + MINS_IN_DAY

        # FIRST DAY
        modified_start = start_time - TRAIN_NUMBER_OF_DAYS * MINS_IN_DAY
        modified_end = end_time - TRAIN_NUMBER_OF_DAYS * MINS_IN_DAY

        if modified_end > 0:
            if modified_start > 0:
                self.work_times.append((modified_start, modified_end))
            else:
                self.work_times.append((0, modified_end))

        # THIS DAY
        self.work_times.append((start_time, end_time))

        # LAST DAY
        modified_start = start_time + TRAIN_NUMBER_OF_DAYS * MINS_IN_DAY
        modified_end = end_time + TRAIN_NUMBER_OF_DAYS * MINS_IN_DAY

        if modified_start < SCHEDULE_NUMBER_OF_DAYS * MINS_IN_DAY:
            if modified_end < SCHEDULE_NUMBER_OF_DAYS * MINS_IN_DAY:
                self.work_times.append((modified_start, modified_end))
            else:
                self.work_times.append((modified_start, SCHEDULE_NUMBER_OF_DAYS * MINS_IN_DAY))

    def add_task(self, start_time, end_time, train_id):
        return self.schedule.add_event(start_time, end_time, {'train_id': train_id})

    def is_available(self, start_time, end_time):
        return self.schedule.is_free(start_time, end_time)

    def export_schedule(self):
        self.schedule.export("train_{}.csv".format(self.id))

    def get_free_slots(self, start_time, end_time):
        free_slots = []
        for (in_time, out_time) in self.work_times:
            free_slots += self.schedule.get_free_slots(max(in_time, start_time), min(out_time, end_time))
        return free_slots


class WasherManager:

    def __init__(self, washer_data):

        self.washers = {}
        for id, washer in washer_data.items():
            self.washers[id] = Washer(id, washer[0], washer[1], washer[2])

    @property
    def washer_list(self):
        return self.washers.keys()

    def get_chaos(self):
        """
        sum of number of events in a washer schedules, lower events means less transitions meaning less chaos
        :return:
        """
        return sum(x.schedule.count for x in self.washers.values())

    def get_available_washers(self, wash_time, start_time, end_time):
        """

        :param wash_time:
        :param start_time:
        :param end_time:
        :return:   An array where each elemenet is -
                ((start_time, end_time), {washer_id1:(start_time1,end_time1),washer_id2:(start_time2,end_time2)...})

        """

        slots = []
        for washer_id, washer in self.washers.items():
            slots = slots + [(x, {washer_id: x}) for x in washer.get_free_slots(start_time, end_time)]

        slots.sort(key=lambda x: x[0][0])

        for x in slots:
            assert (get_slot_wash_time(x) != 0)

        big_slots = [x for x in slots if x[0][1] - x[0][0] >= wash_time]

        small_slots = [x for x in slots if x[0][1] - x[0][0] < wash_time]
        small_slots = combine_small_slots(small_slots, wash_time)
        # small_slots = [x for x in small_slots if x[0][1] - x[0][0] >= wash_time]

        all_slots = big_slots + small_slots

        # Let's try to get all slots at an interval of INTERVAL minutes
        final_slots = []
        for slot in all_slots:
            if get_slot_wash_time(slot) == wash_time:
                final_slots.append(slot)
            else:
                # from the beginning:
                slot_start = slot[0][0]
                while slot_start + wash_time < slot[0][1]:
                    temp_slot = get_slot_of_wash_time_given_start(slot, slot_start, wash_time)
                    if temp_slot:
                        final_slots.append(temp_slot)

                    # slot_end = slot_start + wash_time
                    # slot_temp = [(slot_start, slot_end), get_washers_in_time_slot(slot[1], slot_start, slot_end)]
                    # final_slots.append(slot_temp)
                    slot_start += WASH_INTERVAL

                # from the end:
                slot_end = slot[0][1]
                while slot_end - wash_time > slot[0][0]:
                    temp_slot = get_slot_of_wash_time_given_end(slot, slot_end, wash_time)
                    if temp_slot:
                        final_slots.append(temp_slot)

                    slot_end -= WASH_INTERVAL

        final_slots.sort(key=lambda x: len(x[1]))

        for x in final_slots:
            assert (get_slot_wash_time(x) == wash_time)

        return final_slots

    def assign_washer(self, washer_with_sched, train):
        for washer_id, washer_slot in washer_with_sched[1].items():
            washer = self.washers[washer_id]
            assert (washer.add_task(washer_slot[0], washer_slot[1],
                                    train.id)), "washer_id {}, washer_sched {}, slot: ({}, {}), train_id: {}, washer_with_sched {}".format(
                washer_id, str(washer.schedule.sched), washer_slot[0], washer_slot[1], train.id, str(washer_with_sched))
