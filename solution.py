import pickle
import copy
import random

from config import TASK_ORDER, DEBUG, MAX_OPTS_TO_RETURN, CHAOS_MARGIN, SOLS_TO_PICK, LAST_TASK_INDEX, SOLUTION_FILENAME
from plot_utils import plot_current_state_washer, plot_current_state_lines

final_solutions = []

best_chaos = dict((TASK_ORDER[x], 9999) for x in range(LAST_TASK_INDEX + 1))


def save_solutions(filename, solutions):
    with open(filename, "wb") as f:
        pickle.dump(solutions, f, pickle.HIGHEST_PROTOCOL)


def load_solutions(filename):
    with open(filename, "rb") as f:
        return pickle.load(f)


class Solution:

    def __init__(self, train_manager):
        self.tm = train_manager
        self.current_train_index = 0
        self.current_task_index = 0

    @property
    def current_task(self):
        return TASK_ORDER[self.current_task_index]

    @property
    def chaos(self):
        return dict((x, self.tm.get_task_chaos(x)) for x in TASK_ORDER)

    def no_more_trains(self):
        return self.current_train_index == len(self.tm)

    def is_best_chaos(self, task_index):
        task = TASK_ORDER[task_index]
        chaos = self.tm.get_task_chaos(task)
        if chaos is None:
            return True

        return chaos <= best_chaos[task] * CHAOS_MARGIN[task]

    def task_first_next(self):

        list_of_solutions = []

        if self.no_more_trains():
            final_solutions.append(self)
            # assert (self.tm.is_train_processed(self.current_train_index))
            best_chaos[TASK_ORDER[LAST_TASK_INDEX]] = min(self.tm.get_task_chaos(TASK_ORDER[LAST_TASK_INDEX]),
                                                          best_chaos[TASK_ORDER[LAST_TASK_INDEX]])

            print("Found a solution: {}, current_chaos {}, best_chaos{}".format(len(final_solutions), str(
                self.tm.get_task_chaos(TASK_ORDER[LAST_TASK_INDEX])), str(best_chaos)))

            return []

        options = self.tm.get_options_for_task(self.current_train_index, self.current_task)
        options = options[: min(MAX_OPTS_TO_RETURN[self.current_task], len(options))]

        for opt in options:
            temp_sol = copy.deepcopy(self)
            if (temp_sol.current_train_index > 19):
                x = 1

            temp_sol.tm.do_task_with_option(temp_sol.current_train_index, temp_sol.current_task, opt)

            if temp_sol.tm.is_train_processed(temp_sol.current_train_index):
                print("Train processed {}/{}".format(temp_sol.current_train_index, len(temp_sol.tm)))
                temp_sol.current_task_index = LAST_TASK_INDEX
                temp_sol.current_train_index += 1
            else:
                temp_sol.current_task_index += 1

            # make sure we only add those solution which even at partial solutions are less chaotic then the least full solution
            if temp_sol.is_best_chaos(LAST_TASK_INDEX):
                list_of_solutions.append(temp_sol)

        return list_of_solutions

    def next(self):
        """Summary
        return all valid solutions after doing the next task
        in case the solution is complete and there is no next task to be done
        it stores the solution into a global varaible final_solutions


        Returns:
            List<Solution>:
        """

        if self.current_task in ["IN_STATION", "OUT_STATION", "IDLE"]:
            return self.task_first_next()

        # print(self.current_task)
        # find a train to do the given task
        found_valid_task = False
        while not found_valid_task and not self.no_more_trains():
            # check if this is a valid task
            if self.tm.is_valid_task(self.current_train_index, self.current_task):
                assert (not self.tm.is_task_complete(self.current_train_index,
                                                     self.current_task)), "train_id {}, completed tasks{}".format(
                    self.current_train_index, self.tm.trains[self.current_train_index].complete_task_list)
                found_valid_task = True
            else:
                self.current_train_index += 1

        # make sure we only add those solution which even at partial solutions are less chaotic then the least full solution
        if self.no_more_trains():
            final_solutions.append(self)
            best_chaos[self.current_task] = min(self.tm.get_task_chaos(self.current_task),
                                                best_chaos[self.current_task])
            print("Found a solution: {}, current_chaos {}, best_chaos{}".format(len(final_solutions), str(
                self.tm.get_task_chaos(self.current_task)), str(best_chaos)))
            # if len(final_solutions) > 50:
            #     plot_current_state_lines(self.tm.pm.platforms)
            #     cc = 1

            return []

        list_of_solutions = []

        options = self.tm.get_options_for_task(self.current_train_index, self.current_task)

        options = options[: min(MAX_OPTS_TO_RETURN[self.current_task], len(options))]

        for opt in options:
            temp_sol = copy.deepcopy(self)
            temp_sol.tm.do_task_with_option(self.current_train_index, self.current_task, opt)

            # from all the solutions only add solutions which are
            if temp_sol.is_best_chaos(self.current_task_index):
                temp_sol.current_train_index += 1
                list_of_solutions.append(temp_sol)

        # self.current_train_index += 1

        return list_of_solutions


def find_solution_rec(possible_solutions):
    """
    give a list of possible_solutions, it iterates over all these solutions depth first
    till it meets end criteria where it return empty list (which is termination)

    end criteria is possible in two ways
    1. no possible solution, terminate and retunrn empty list
    2. save the sol in "FINAL_SOLUTION" variable, terminate and return empty list

    :param possible_solutions:
    :return:
    """
    if len(possible_solutions) == 0:
        return []

    list_of_solutions = []

    for i, sol in enumerate(possible_solutions):
        if sol.current_train_index == 0:
            print("processing  {} / {} base solutions".format(i + 1, len(possible_solutions)))

        temp_list_of_sols = sol.next()
        solutions = find_solution_rec(temp_list_of_sols)

        if DEBUG and not len(final_solutions):
            cur_train = sol.tm.trains[sol.current_train_index]
            if sol.current_task == "WASHER":
                plot_current_state_washer(sol, cur_train.schedule)
            elif sol.current_task == "WASHLINE":
                plot_current_state_lines(sol.tm.pm.washlines, cur_train.schedule)
            else:
                print(str(cur_train.id), str(cur_train.schedule))
                if sol.current_train_index > 0:
                    print(sol.current_train_index, sol.current_task)
                    plot_current_state_lines(sol.tm.pm.all_lines, cur_train.schedule)

        list_of_solutions = list_of_solutions + solutions

    return list_of_solutions


def find_solution(train_manager, starting_task_index=0):
    global final_solutions
    i = starting_task_index

    if i > 0:
        list_of_sol = load_solutions(SOLUTION_FILENAME.format(TASK_ORDER[i - 1]))
        print("Starting with {} solutions for task {}".format(len(list_of_sol), TASK_ORDER[i]))

    while (i <= LAST_TASK_INDEX):
        final_solutions = []
        task = TASK_ORDER[i]

        if task == "WASHER":
            list_of_sol = find_solution_rec([Solution(train_manager)])

        elif task == "WASH_LINE":
            # sort solutions and pick top x solutions
            list_of_sol.sort(key=lambda sol: sol.tm.get_secondary_chaos(TASK_ORDER[i - 1]))
            if len(list_of_sol) > SOLS_TO_PICK[task]:
                list_of_sol = list_of_sol[:SOLS_TO_PICK[task]]

            for sol in list_of_sol:
                sol.current_train_index = 0
                sol.current_task_index = i

            list_of_sol = find_solution_rec(list_of_sol)

        elif task == "IN_STATION":
            if len(list_of_sol) > SOLS_TO_PICK[task]:
                list_of_sol = random.sample(list_of_sol, SOLS_TO_PICK[task])

            for sol in list_of_sol:
                sol.tm.sort_trains_on_priority(reverse=True)
                sol.current_train_index = 0
                sol.current_task_index = i

            # plot_current_state_lines(list_of_sol[0].tm.pm.all_lines, None)

            list_of_sol = find_solution_rec(list_of_sol)

        assert (len(list_of_sol) == 0)

        list_of_sol = [x for x in final_solutions if x.is_best_chaos(i)]
        save_solutions(SOLUTION_FILENAME.format(task), list_of_sol)
        print("Found {} solution for task {}".format(len(list_of_sol), TASK_ORDER[i]))

        i += 1

        # def progress(current_task, current_train, )
