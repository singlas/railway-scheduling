from platformManager import PlatformManager
from washManager import WasherManager
from trainManager import TrainManager
from solution import find_solution

import csv

trains = []

with open('data/trains.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        # print(row)
        row['in_time'] = int(row['in_time'])
        row['out_time'] = int(row['out_time'])
        row['length'] = int(row['length'])
        trains.append(row)

platform_lines = {

    "1": {"length": 24, "connect": ["1W", "2W", "3W", "4W"]},
    "2": {"length": 23, "connect": ["1W", "2W", "3W", "4W"]},
    "3": {"length": 24, "connect": ["1W", "2W", "3W", "4W"]},
    "4": {"length": 23, "connect": ["1W", "2W", "3W", "4W"]},
    "5": {"length": 24, "connect": ["1W", "5W", "6W"]},
    "6": {"length": 24, "connect": ["5W", "6W"]},
    "7": {"length": 17, "connect": ["5W", "6W"]},
    "1A": {"length": 8, "connect": []},
}

wash_lines = {
    "1W": {"length": 24, "connect": ["1", "2", "3", "4", "5"], "supports": ["ICF"]},
    "2W": {"length": 22, "connect": ["1", "2", "3", "4", ], "supports": ["ICF"]},
    "3W": {"length": 21, "connect": ["1", "2", "3", "4"], "supports": ["ICF", "LHB"]},
    "4W": {"length": 18, "connect": ["1", "2", "3", "4"], "supports": ["ICF", "LHB"]},
    "5W": {"length": 24, "connect": ["5", "6", "7"], "supports": ["ICF", "LHB"]},
    "6W": {"length": 24, "connect": ["5", "6", "7"], "supports": ["ICF", "LHB"]},
}

stabling = {
    "Loco Maina": {"length": 24, "connect": ["1", "2"], "supports": ["ICF"]},
    "Line 11": {"length": 24, "connect": ["5", "6"], "supports": ["ICF"]},
    "Line 13": {"length": 21, "connect": ["7", "5", "6"], "supports": ["ICF"]},
    "Line 14": {"length": 21, "connect": ["7", "5", "6"], "supports": ["ICF"]},
    "Line 5H": {"length": 17, "connect": ["5", "6"], "supports": ["ICF"]},
    "Line 6H": {"length": 17, "connect": ["5", "6"], "supports": ["ICF"]},
    "Line 7H": {"length": 17, "connect": ["5", "6"], "supports": ["ICF"]},
    "Attori Line": {"length": 24, "connect": ["1", "2", "3", "4"], "supports": ["ICF"]},
}

wash_batches = {
    "Am": [8, 16, 'all'],
    "Ae": [16, 0, 'all'],
    "An": [0, 8, 'all'],
    "As": [22, 6, 'all'],
    "Bm": [8, 16, 'all'],
    "Bs": [9, 17, 'all'],
    "Bn": [21, 5, 'all'],
    "Cm": [10, 18, 'all'],
    "Dm": [10, 18, 'all'],
    "E": [15, 23, 'all'],
    "Gb": [20, 4, 'F,S,S'],
}


def main():
    pm = PlatformManager(platform_lines, wash_lines, stabling)
    wm = WasherManager(wash_batches)
    tm = TrainManager(trains, pm, wm)

    find_solution(tm, 2)


main()

# 23.54 LHB
# 21.3 ICF
