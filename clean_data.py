import csv

station_name = "ASR"
trains = []

train_len = {}

with open('data/train_data_v2.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        trains.append(row)

with open('data/train_len.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        train_len[row['T.No.']] = row

last_train = None
new_trains = []


def get_correct_train_id(train_no, ret_train_no, link_number):
    if link_number:
        return link_number
    else:
        train_no = train_no.split(".")[0]
        ret_train_no = ret_train_no.split(".")[0]

        return train_no if train_no in train_len else ret_train_no

def time_in_mins(tm):
    t = tm.strip().split(":")
    return int(t[0]) * 60 + int(t[1])

def assign_train_given_pair(arr_train, dep_train):
    assert (arr_train['Dest'] == station_name)
    assert (dep_train['Src'] == station_name)

    corr_train_no = get_correct_train_id(arr_train['T.No.'], dep_train['T.No.'], arr_train['train_link_number'])

    new_train = {'in_train': arr_train['T.No.'],  # prev train's destination is our station
                 'out_train': dep_train['T.No.'],

                 'in_time': time_in_mins(arr_train['Arr']),
                 'out_time': time_in_mins(dep_train['Dep']),

                 'pf': dep_train['Pf'],
                 'type': dep_train['Type'],
                 'length': int(train_len[corr_train_no]['Length']),
                 'rake_type': train_len[corr_train_no]['Rake_type']
                 }

    if new_train['in_time'] > new_train['out_time']:
        new_train['out_time'] += 24 * 60

    if train['Sunday'] == "S":
        new_trains.append(new_train)

# handle_dmu_rake_links:

#List all unique Link numbers
link_numbers = set([x['train_link_number'] for x in trains])
for link_number in link_numbers:
    if link_number:
        all_trains_for_link = [x for x in trains if x['train_link_number'] == link_number]
        all_trains_for_link.sort(key= lambda trn: int(trn['link_order'].strip()))

        prev_train = all_trains_for_link[-1]
        for i, train in enumerate(all_trains_for_link):
            assert (not train['R.T.No.'])
            if prev_train['Dest'] != station_name:
                prev_train = train

            elif i != 0 and int(train['link_order'].strip()) != int(prev_train['link_order'].strip()) + 1:
                #if it is neither the first train
                #nor it is next to the previous train, i.e. we are missing a train - skip ahead
                prev_train = train

            else:
                assign_train_given_pair(prev_train, train)

trains = [x for x in trains if x['train_link_number'].strip() == ""]
for train in trains:
    if last_train is None:
        if train['R.T.No.'].strip() != "":
            last_train = train

    else:
        if (last_train['T.No.'] != train['R.T.No.']):
            print("Train no. {} and Return train no. {} not in order".format(last_train['T.No.'], train['T.No.']))

        assert (last_train['T.No.'] == train['R.T.No.'])

        # if train is the one arriving to parent station
        if train['Src'] == 'ASR':
            assign_train_given_pair(last_train, train)
        else:
            assign_train_given_pair(train, last_train)

        last_train = None

keys = new_trains[0].keys()

with open('data/trains.csv', 'w', encoding='utf8', newline='') as output_file:
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    dict_writer.writerows(new_trains)
