
def get_slot_wash_time(slot):
    return sum([t[1] - t[0] for t in slot[1].values()])


def find_all_combinations(starting_slot, list_of_slots, wash_time):
    if len(list_of_slots) == 0:
        return [starting_slot] if get_slot_wash_time(starting_slot) > wash_time else []

    all_valid_slots = []

    for slot in list_of_slots:
        if starting_slot[0][1] <= slot[0][0]:
            new_starting_slot = [(starting_slot[0][0], slot[0][1]), starting_slot[1]]
            new_starting_slot[1].update(slot[1])
            new_list_of_slots = [x for x in list_of_slots if x[0][0] > slot[0][0]]
            all_valid_slots += find_all_combinations(new_starting_slot, new_list_of_slots, wash_time)

    return all_valid_slots


def combine_small_slots(small_slots, wash_time):
    # combine_happened = True

    all_valid_slots = []
    for slot in small_slots:
        new_list_of_slots = [x for x in small_slots if x[0][0] > slot[0][0]]
        all_valid_slots += find_all_combinations(slot, new_list_of_slots, wash_time)

    return all_valid_slots


def get_slot_of_wash_time_given_start(slot, slot_start, wash_time):
    actual_slot_washer_sched = {}

    slot_washer_sched = [(washer_id, sched_entry) for washer_id, sched_entry in slot[1].items()]

    slot_washer_sched.sort(key=lambda x: x[1][0])

    allocated_wash_time = 0
    slot_end = slot_start

    for (washer_id, sched_entry) in slot_washer_sched:
        unallocated_wash_time = wash_time - allocated_wash_time
        if unallocated_wash_time > 0 and sched_entry[0] >= slot_start:
            this_allocation = min(sched_entry[1] - sched_entry[0], unallocated_wash_time)

            slot_end = sched_entry[0] + this_allocation
            allocated_wash_time += this_allocation
            actual_slot_washer_sched[washer_id] = (sched_entry[0], slot_end)

    if allocated_wash_time == wash_time:
        return [(slot_start, slot_end), actual_slot_washer_sched]

    return None


def get_slot_of_wash_time_given_end(slot, slot_end, wash_time):
    actual_slot_washer_sched = {}

    slot_washer_sched = [(washer_id, sched_entry) for washer_id, sched_entry in slot[1].items()]

    slot_washer_sched.sort(key=lambda x: x[1][1], reverse=True)

    allocated_wash_time = 0
    slot_start = slot_end

    for (washer_id, sched_entry) in slot_washer_sched:

        if allocated_wash_time < wash_time and sched_entry[1] <= slot_end:
            unallocated_wash_time = wash_time - allocated_wash_time
            this_allocation = min(sched_entry[1] - sched_entry[0], unallocated_wash_time)

            slot_start = sched_entry[1] - this_allocation
            allocated_wash_time += this_allocation
            actual_slot_washer_sched[washer_id] = (slot_start, sched_entry[1])

    if allocated_wash_time == wash_time:
        return [(slot_start, slot_end), actual_slot_washer_sched]

    return None
