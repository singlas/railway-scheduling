import csv

from config import TRAIN_NUMBER_OF_DAYS, SCHEDULE_NUMBER_OF_DAYS, MINS_IN_DAY


class Schedule:
    def __init__(self, id):
        self.sched = []
        self.count = 0
        self.id = id

    # @property
    # def count(self):
    #     return len(self.sched)

    def add_event_basic(self, start_time, end_time, event_details):
        slot = self.get_free_slot(start_time, end_time)
        event = {"start": start_time,
                 "end": end_time, "details": event_details}

        if slot >= 0:
            self.sched.insert(slot, event)
            return True

        else:
            return False

    def add_event(self, start_time, end_time, event_details):
        event_added = self.add_event_basic(start_time, end_time, event_details)

        if not event_added:
            return False

        # if applicable, add for the round-robin adjustment for last and first day
        # LAST DAY
        modified_start = start_time + TRAIN_NUMBER_OF_DAYS * MINS_IN_DAY
        modified_end = end_time + TRAIN_NUMBER_OF_DAYS * MINS_IN_DAY

        if modified_start < SCHEDULE_NUMBER_OF_DAYS * MINS_IN_DAY:
            if modified_end < SCHEDULE_NUMBER_OF_DAYS * MINS_IN_DAY:
                self.add_event_basic(modified_start, modified_end, event_details)
            else:
                self.add_event_basic(modified_start, SCHEDULE_NUMBER_OF_DAYS * MINS_IN_DAY, event_details)

        # FIRST DAY
        modified_start = start_time - TRAIN_NUMBER_OF_DAYS * MINS_IN_DAY
        modified_end = end_time - TRAIN_NUMBER_OF_DAYS * MINS_IN_DAY

        if modified_end > 0:
            if modified_start > 0:
                self.add_event_basic(modified_start, modified_end, event_details)
            else:
                self.add_event_basic(0, modified_end, event_details)

        self.count += 1
        return True

    def is_free(self, start_time, end_time):
        return self.get_free_slot(start_time, end_time) >= 0

    def get_free_slot(self, start_time, end_time):
        if len(self.sched) == 0:
            return 0

        i = 0

        while i < len(self.sched):
            # checking for special cases
            if i == 0:
                if start_time < end_time <= self.sched[i]["start"]:
                    return 0

            elif self.sched[i - 1]["end"] <= start_time < end_time <= self.sched[i]["start"]:
                return i

            i += 1

        if self.sched[i - 1]["end"] <= start_time < end_time:
            return i

        return -1

    def export(self, filename):

        with open("data/sched/{}".format(filename), 'w', encoding='utf8', newline='') as output_file:
            writer = csv.writer(output_file)
            writer.writerow(["start_time", "end_time", "train no"])
            for line in self.sched:
                writer.writerow([line['start'], line['end'],
                                 line['details']['in_train']])

    def get_free_slots(self, start_time, end_time):

        free_slots = []
        prev_slot_end = start_time
        for event in self.sched:

            if event['start'] <= start_time:
                # just update the previous end time, no free slot found yet
                prev_slot_end = max(event['end'], start_time)

            elif event['start'] < end_time:
                # Found a free slot which ends with this event's start
                free_slots.append([prev_slot_end, event['start']])
                prev_slot_end = event['end']

            else:
                # this event is outside our window, no need to look further
                break

        if prev_slot_end < end_time:
            # this free window would not be caught in the loop as this doesn't end in an event
            free_slots.append([prev_slot_end, end_time])

        return [x for x in free_slots if x[1] - x[0] > 0]

    def min_gap(self):

        if self.count < 2:
            return -1


        start = self.sched[0]['end']

        min_gap = SCHEDULE_NUMBER_OF_DAYS * MINS_IN_DAY

        for i, event in enumerate(self.sched[1:]):

            if event['start'] - start < min_gap:
                min_gap = event['start'] - start

            start = event['end']

        return min_gap