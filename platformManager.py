from config import LINE_TYPE_PLATFORM, LINE_TYPE_WASHLINE, LINE_TYPE_STABLING, SCHEDULE_NUMBER_OF_DAYS, MINS_IN_DAY
from schedule import Schedule
from statistics import mean, variance


class Platform:
    def __init__(self, id, length, connections, supports, plt_type):
        self.id = id
        self.size = length
        self.connections = connections
        self.supports = supports

        self.type = plt_type
        self.schedule = Schedule(id)

    def __len__(self):
        """
        A string representation of this map
        :return: A string representing this map
        """
        return self.size

    def schedule_train(self, arv_time, end_time, event_details):
        self.schedule.add_event(arv_time, end_time, event_details)

    def is_available(self, arv_time, end_time):
        return self.schedule.is_free(arv_time, end_time)

    def export_schedule(self):
        self.schedule.export("schedule_pf{}.csv".format(self.id))

    def train_count(self):
        return self.schedule.count

    def priority_score_w(self, train):
        score = 0

        if train.rake_type == "ICF" and "LHF" in self.supports:
            score += 3

        score += self.size - len(train)

        if self.schedule.count < 2:
            score -= 2

        elif self.schedule.min_gap() < 100:
            score += 2

        # if self.schedule.count < 2:
        #     return -1 * SCHEDULE_NUMBER_OF_DAYS * MINS_IN_DAY
        # else:
        #     return -1 * self.schedule.min_gap()

        return score

    def priority_score(self, train, stabling_lines):

        score = 0

        if self.schedule.count < 2:
            score -= 2

        score += .25*(self.size - len(train))


        # if more washlines less priority
        score += len(self.connections)

        # for IN STATION and OUT STATION
        if stabling_lines:
            stabling_lines_count = sum([1 for x in stabling_lines if self.id in x.connections])
            wait_time = train.out_time - train.in_time - train.wash_time - 60
            score -= stabling_lines_count*wait_time/120

        if "IN_STATION" in train.schedule and train.schedule['IN_STATION'] == self.id:
            score -= 2

        return score

    def min_gap(self):
        return self.schedule.min_gap()

    def get_available_sub_slot_from_start(self, start_time, end_time):
        free_slots = self.schedule.get_free_slots(start_time, end_time)
        start_slots = [x for x in free_slots if x[0] == start_time]

        if len(start_slots):
            assert len(start_slots) == 1
            return start_slots[0]

        return None

    def get_available_sub_slot_from_end(self, start_time, end_time):
        free_slots = self.schedule.get_free_slots(start_time, end_time)
        end_slots = [x for x in free_slots if x[1] == end_time]

        if len(end_slots):
            assert len(end_slots) == 1
            return end_slots[0]

        return None


# def get_wait_time_variance(self):
#     return 1
#


class PlatformManager:

    def __init__(self, platforms, wash, stable):
        self.platforms = {}
        self.washlines = {}
        self.stabling = {}

        for id, platform_data in platforms.items():
            self.platforms[id] = Platform(id, platform_data['length'], platform_data['connect'], [], LINE_TYPE_PLATFORM)

        for id, platform_data in wash.items():
            self.washlines[id] = Platform(id, platform_data['length'], platform_data['connect'],
                                          platform_data['supports'], LINE_TYPE_WASHLINE)

        for id, platform_data in stable.items():
            self.stabling[id] = Platform(id, platform_data['length'], platform_data['connect'],
                                         [], LINE_TYPE_STABLING)

    @property
    def all_lines(self):
        all_lines = {}
        all_lines.update(self.platforms)
        all_lines.update(self.washlines)
        all_lines.update(self.stabling)

        return all_lines

    @property
    def platform_list(self):
        return self.platforms.keys()

    @property
    def washline_list(self):
        return self.washlines.keys()

    @property
    def stabling_list(self):
        return self.stabling.keys()

    def export_schedule(self):
        for id in self.platforms:
            self.platforms[id].export_schedule()

    def assign_line(self, index, in_time, out_time, train_id, line_type, event_details=None):
        if event_details is None:
            event_details = {}

        event_details['train_id'] = train_id

        if line_type == LINE_TYPE_WASHLINE:
            line = self.washlines[index]
        if line_type == LINE_TYPE_PLATFORM:
            line = self.platforms[index]
        if line_type == LINE_TYPE_STABLING:
            line = self.stabling[index]

        line.schedule_train(in_time, out_time, event_details)

    def get_available_washlines(self, train, start_time, end_time):
        # remove locked washlines
        available_washlines = [x for x in self.washline_list if self.washlines[x].is_available(start_time, end_time)]

        # remove smaller washlines
        available_washlines = [x for x in available_washlines if len(self.washlines[x]) >= train.size]

        # remove lines that don't support this train's rake type
        available_washlines = [x for x in available_washlines if train.rake_type in self.washlines[x].supports]

        available_washlines.sort(key=lambda x: self.washlines[x].priority_score_w(train))
        # optimize
        # available_washlines = []

        return available_washlines

    def get_available_platforms(self, arr_time, dep_time, train):
        # remove locked platforms
        available_platforms = [x for x in self.platform_list if self.platforms[x].is_available(arr_time, dep_time)]

        # remove smaller platforms
        available_platforms = [x for x in available_platforms if len(self.platforms[x]) >= train.size]

        # if assigned to a washline, check for connection with the platform
        if 'washline' in train.schedule:
            available_platforms = [x for x in available_platforms
                                   if train.schedule['washline'] in self.platforms[x].connections]

        available_platforms.sort(key=lambda x: self.platforms[x].priority_score(train, self.stabling.values()))

        return available_platforms

    def get_available_stabling_lines(self, start_time, end_time, train, connect_to_platform_index):

        available_lines = [x for x in self.stabling_list if
                           self.stabling[x].is_available(start_time, end_time)]

        # remove smaller lines
        available_lines = [x for x in available_lines if len(self.stabling[x]) >= train.size]

        # if assigned to a washline, check for connection with the platform

        for connecting_platform in connect_to_platform_index:
            available_lines = [x for x in available_lines if connecting_platform in self.stabling[x].connections]

        available_lines.sort(key=lambda x: self.stabling[x].priority_score(train,None))

        return available_lines

    def get_chaos(self, plt_type):
        pfms = {}

        if plt_type == LINE_TYPE_WASHLINE:
            pfms = self.washlines

        if plt_type == LINE_TYPE_PLATFORM:
            pfms = self.platforms

        min_gap = [pfm.min_gap() for pfm in pfms.values()]

        min_gap = [x for x in min_gap if x != -1]
        if len(min_gap):
            return -1 * round(mean(min_gap))
        else:
            return None
