import matplotlib.pyplot as plt

from config import SCHEDULE_NUMBER_OF_DAYS, MINS_IN_DAY


def plot_current_state_lines(lines, prob_train_sched=None):
    washer_count = 0
    for id, platform in lines.items():
        washer_count += 1

        plt.annotate(id, xy=(0, washer_count+.5))

        # plot washer filled schedule in blue

        x_free = []
        y_free = []
        x_filled = []
        y_filled = []

        prev_end = 0
        for event in platform.schedule.sched:
            for t in range(prev_end, event["start"], 5):
                x_free.append(t)
                y_free.append(washer_count)
            for t in range(event["start"], event["end"], 5):
                x_filled.append(t)
                y_filled.append(washer_count)

            plt.annotate(event['details']['train_id'], xy=(event['start'], washer_count))

        for t in range(prev_end, SCHEDULE_NUMBER_OF_DAYS * MINS_IN_DAY, 5):
            x_free.append(t)
            y_free.append(washer_count)

        plt.plot(x_free, y_free, 'g--', x_filled, y_filled, 'b^')

    if prob_train_sched:
        # plot the problem train in yellow
        x_work = []
        y_work = []

        for t in range(prob_train_sched['in_station_time'][1], prob_train_sched['out_station_time'][0], 5):
            x_work.append(t)
            y_work.append(washer_count + 1)
        plt.plot(x_work, y_work, 'ys')
        # plt.annotate(prob_train_sched['train_id'], xy=(event['start'], washer_count))

    plt.xlabel = "time_in_mins"
    plt.ylabel = "washer_count"
    plt.show()

def plot_current_state_washer(sol, prob_train_sched=None):
    washer_count = 0
    for id, washer in sol.tm.wm.washers.items():
        washer_count += 1

        # plot washer availability in red
        x_work = []
        y_work = []
        for work_time in washer.work_times:
            for t in range(work_time[0], work_time[1], 5):
                x_work.append(t)
                y_work.append(washer_count)
            plt.plot(x_work, y_work, 'rs')

        # plot washer filled schedule in blue

        x_free = []
        y_free = []
        x_filled = []
        y_filled = []

        prev_end = 0
        for event in washer.schedule.sched:
            for t in range(prev_end, event["start"], 5):
                x_free.append(t)
                y_free.append(washer_count)
            for t in range(event["start"], event["end"], 5):
                x_filled.append(t)
                y_filled.append(washer_count)

            plt.annotate(event['details']['train_id'], xy=(event['start'], washer_count))

        for t in range(prev_end, SCHEDULE_NUMBER_OF_DAYS * MINS_IN_DAY, 5):
            x_free.append(t)
            y_free.append(washer_count)

        plt.plot(x_free, y_free, 'g--', x_filled, y_filled, 'b^')

    if prob_train_sched:
        # plot the problem train in yellow
        x_work = []
        y_work = []

        for t in range(prob_train_sched['in_station_time'][1], prob_train_sched['out_station_time'][0], 5):
            x_work.append(t)
            y_work.append(washer_count + 1)
        plt.plot(x_work, y_work, 'ys')

    plt.xlabel = "time_in_mins"
    plt.ylabel = "washer_count"
    plt.show()
